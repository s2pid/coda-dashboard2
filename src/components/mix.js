import React from 'react';
import {Line} from 'react-chartjs-2';


// TODO: Get data from API/PHP
// Month data
// value data for current year
// value data for last year
// point target reached color data for current year
// point target reached color data for last year
const data = {
    labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec', ''],
    datasets: [{
        label: '2017 Balance: £75k',
        type:'line',
        data: [15000, 23000, 31000, 31000, 32000, 40000, 53000, 72000, 78000, 80000, 60000, 30000],
        fill: false,

        borderColor: '#589CE3',
        borderWidth: 2,

        pointBorderWidth: 2,
        pointHoverBorderWidth: 2,
        pointRadius: 6,
        pointHoverRadius: 6,
        pointBackgroundColor: '#ffffff',
        pointBorderColor: [
            '#589CE3',
            '#589CE3',
            '#FF0000',
            '#589CE3',
            '#589CE3',
            '#589CE3',
            '#589CE3',
            '#589CE3',
            '#589CE3',
            '#589CE3',
            '#589CE3',
            '#589CE3',
            '#589CE3',
        ],
        pointHoverBackgroundColor: '#ffffff',
        pointHoverBorderColor: [
            '#589CE3',
            '#589CE3',
            '#FF0000',
            '#589CE3',
            '#589CE3',
            '#589CE3',
            '#589CE3',
            '#589CE3',
            '#589CE3',
            '#589CE3',
            '#589CE3',
            '#589CE3',
            '#589CE3',
        ],
        pointHitRadius: 10,
    },{
        label: '2016 Balance: £63k',
        type:'line',
        data: [51000, 65000, 40000, 49000, 60000, 37000, 40000, 15000, 85000, 78000, 66000, 25000],
        fill: false,

        borderColor: '#EC932F',
        borderWidth: 2,

        pointBorderWidth: 2,
        pointHoverBorderWidth: 2,
        pointRadius: 6,
        pointHoverRadius: 6,
        pointBackgroundColor: '#ffffff',
        pointBorderColor: '#EC932F',
        pointHoverBackgroundColor: '#ffffff',
        pointHoverBorderColor: '#EC932F',
        pointHitRadius: 10,
    },{
        label: 'Target',
        type:'line',
        data: [21000, 35000, 43000, 58000, 56000, 69000, 85000, 80000, 66000, 36000, 12000, 5000],
        fill: true,

        borderWidth: 1,
        borderColor: 'rgba(215, 215, 215, 0.3)', //#D7D7D7
        backgroundColor: 'rgba(215, 215, 215, 0.3)', //#D7D7D7

        pointBorderWidth: 0,
        pointHoverBorderWidth: 0,
        pointRadius: 0,
        pointHoverRadius: 0,
        pointHitRadius: 10,
    }]
};

const options = {
    responsive: true,
    tooltips: {
        mode: 'point',
        backgroundColor: '#FFFFFF',
        titleFontFamily: "'Open Sans', sans-serif",
        titleFontSize: 13,
        titleFontColor: '#000000',
        bodyFontFamily: "'Open Sans', sans-serif",
        bodyFontSize: 13,
        bodyFontColor: '#000000',

        xPadding: 35,
        yPadding: 15,
        cornerRadius: 3
    },
    tooltipTemplate: "<%= £value %>",
    elements: {
        line: {
            fill: false,
            tension: 0,
        }
    },
    legend: {
        display: false
    },
    scales: {
        xAxes: [
            {
                display: true,
                gridLines: {
                    display: true,
                    color: '#DCDCDC',
                    drawBorder: false,
                },
                labels: {
                    show: true
                },
            }
        ],
        yAxes: [
            {
                type: 'linear',
                display: true,
                position: 'left',
                gridLines: {
                    display: true,
                    color: '#DCDCDC',
                    drawBorder: false,
                },
                labels: {
                    show: true
                },
            }
        ]
    }
};

export default React.createClass({
    displayName: 'MixExample',

    render() {
        return (
            <div>
                <Line
                    data={data}
                    options={options}
                />
            </div>
        );
    }
});