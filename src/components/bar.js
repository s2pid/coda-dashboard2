import React from 'react';
import {Bar} from 'react-chartjs-2';

const data = {
    labels: ['Hollywood undead', 'Trapt', 'Spineshank', 'Thirty seconds to mars', 'Slipknot', 'Dope', 'Asking Aleksandria', 'Bullet for the valentine', 'Godsmack', 'Linking park'],
    datasets: [
        {
            label: 'Number of contracts per artist (Top 10)',
            data: [85, 76, 66, 62, 56, 50, 49, 32, 28, 15],

            backgroundColor: '#FF5751',
            hoverBackgroundColor: '#FF5751',
            borderColor: '#FF5751',
            hoverBorderColor: '#FF5751',
            borderWidth: 1,
        }
    ]
};

const options = {
    responsive: true,
    tooltips: {
        mode: 'point',
        backgroundColor: '#FFFFFF',
        titleFontFamily: "'Open Sans', sans-serif",
        titleFontSize: 13,
        titleFontColor: '#000000',
        bodyFontFamily: "'Open Sans', sans-serif",
        bodyFontSize: 13,
        bodyFontColor: '#000000',

        xPadding: 35,
        yPadding: 15,
        cornerRadius: 3
    },
    legend: {
        display: false
    },
};

export default React.createClass({
    displayName: 'BarExample',

    render() {
        return (
            <div>
                <Bar
                    data={data}
                    options={options}
                />
            </div>
        );
    }
});