import React from 'react';
import {Doughnut} from 'react-chartjs-2';

const data = {
    labels: [
        'Overdue',
        'Overpayment',
        'Complete',
        'Canceled',
    ],
    datasets: [{
        data: [18, 22, 40, 20],
        backgroundColor: [
            '#FF5751',
            '#1E88E5',
            '#7ED321',
            '#F5A623',
        ],
        hoverBackgroundColor: [
            '#FF5751',
            '#1E88E5',
            '#7ED321',
            '#F5A623',
        ]
    }]
};

const options = {
    responsive: true,
    tooltips: {
        mode: 'point',
        backgroundColor: '#FFFFFF',
        titleFontFamily: "'Open Sans', sans-serif",
        titleFontSize: 13,
        titleFontColor: '#000000',
        bodyFontFamily: "'Open Sans', sans-serif",
        bodyFontSize: 13,
        bodyFontColor: '#000000',

        xPadding: 35,
        yPadding: 15,
        cornerRadius: 3
    },
    legend: {
        display: false
    },
    cutoutPercentage: 80,
    height: 250
};

export default React.createClass({
    displayName: 'DoughnutExample',

    render() {
        return (
            <div>
                <Doughnut
                    data={data}
                    options={options}
                />
            </div>
        );
    }
});