import React, { Component } from 'react';
import './App.css';

import DoughnutExample from './components/doughnut';
import DynamicDoughnutExample from './components/dynamic-doughnut';
import PieExample from './components/pie';
import LineExample from './components/line';
import BarExample from './components/bar';
import HorizontalBarExample from './components/horizontalBar';
import RadarExample from './components/radar';
import PolarExample from './components/polar';
import BubbleExample from './components/bubble';
import ScatterExample from './components/scatter';
import MixedDataExample from './components/mix';
import RandomizedDataLineExample from './components/randomizedLine';
import LegendOptionsExample from './components/legend-options';
import LegendHandlersExample from './components/legend-handlers';


const preloader = {
    html: {
        overdue: '<h5>Overdue: <b>25</b></h5>\n' +
        '<div class="progress">\n' +
            '<div class="progress-bar bg-danger" role="progressbar" style="width: 18%;" aria-valuenow="18" aria-valuemin="0" aria-valuemax="100">18%</div>\n' +
        '</div>',
        overpayment: '<div class="progress">\n' +
            '<div class="progress-bar bg-info" role="progressbar" style="width: 22%;" aria-valuenow="22" aria-valuemin="0" aria-valuemax="100">22%</div>\n' +
        '</div>',
        complete: '<div class="progress">\n' +
            '<div class="progress-bar bg-success" role="progressbar" style="width: 40%;" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100">40%</div>\n' +
        '</div>',
        canceled: '<div class="progress">\n' +
            '<div class="progress-bar bg-warning" role="progressbar" style="width: 20%;" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">20%</div>\n' +
        '</div>',
    },
};

class App extends Component {
    render() {
        return (
            <div>
                <h1>Commission over time</h1>
                <div className="row">
                    <div className="col">
                        <div className="box">
                            <div className="col p-5">
                                <MixedDataExample />
                            </div>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-6">
                        <h1>Contracts information</h1>
                    </div>
                    <div className="col-6">
                        <button className="btn btn-primary">Last year</button>
                        <button className="btn btn-primary">Last month</button>
                        <button className="btn btn-primary">Last week</button>
                    </div>
                </div>
                <div className="row">
                    <div className="col-4">
                        <div className="box">
                            first
                        </div>
                    </div>
                    <div className="col-4">
                        <div className="box">
                            first
                        </div>
                    </div>
                    <div className="col-4">
                        <div className="box">
                            first
                        </div>
                    </div>
                </div>
                <h1>Number of contracts per territory</h1>
                <div className="row">
                    <div className="col-8">
                        <div className="box">
                            map block
                        </div>
                    </div>
                    <div className="col-4">
                        <div className="box">
                            country list block
                        </div>
                    </div>
                </div>
                <h1>Number of contracts per artist in your roster (Top 10)</h1>
                <div className="row">
                    <div className="col">
                        <div className="box">
                            <div className="col p-5">
                                <BarExample />
                            </div>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-6">
                        <h1>Number of contracts</h1>
                    </div>
                    <div className="col-6">
                        <h1>Artists with no bookings</h1>
                    </div>
                </div>
                <div className="row">
                    <div className="col-8">
                        <div className="box">
                            <div className="col-6">
                                <DoughnutExample />
                            </div>
                            <div className="col-6">
                                <div className="row">
                                    <div className="col-6" dangerouslySetInnerHTML={ {__html: preloader.html.overdue} }></div>
                                    <div className="col-6" dangerouslySetInnerHTML={ {__html: preloader.html.overpayment} }></div>
                                    <div className="col-6" dangerouslySetInnerHTML={ {__html: preloader.html.complete} }></div>
                                    <div className="col-6" dangerouslySetInnerHTML={ {__html: preloader.html.canceled} }></div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div className="col-4">
                        <div className="box">
                            <div className="col-6">
                        contry list block
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        );
    }
}

export default App;
